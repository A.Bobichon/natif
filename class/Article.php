<?php

class Article {
    private $title;
    private $body;

    public function getTitle(){
        return htmlspecialchars($this->title);
    }

    public function getBody(){
        return htmlspecialchars($this->body);
    }

    public function setTitle($title){
        $this->title = $title;
        return $this->title;
    }

    public function setBody($body){
        $this->body = $body;
        return $this->body;
    }
};


?>