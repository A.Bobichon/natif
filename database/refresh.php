<?php

$path = __DIR__ . '/database.sqlite';

// Delete the database.sql
if(file_exists($path)) unlink($path);

// Do the link whit the bdd sqlite 
$pdo = new PDO("sqlite:$path");

var_dump($pdo);

$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$query = $pdo->prepare('
    CREATE TABLE posts (
        id  INTEGER PRIMARY KEY,
        title VARCHAR(255) NOT NULL,
        body TEXT NOT NULL
    )

');

$query->execute();