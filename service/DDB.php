<?php

    function connectDDB($way){
        $path = __DIR__. $way .'/database/database.sqlite';

        $pdo = new PDO("sqlite:$path");
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $pdo;
    }

?>